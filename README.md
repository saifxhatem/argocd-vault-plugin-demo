# ArgoCD Vault Plugin Demo


## Requirements:
1. `kubectl`
2. `helm`
3. `argocd-cli`

***
## 1. ArgoCD

### ArgoCD Installation:
<br>

1. Create namespace

`$ kubectl create namespace argocd`

2. Apply the argocd manifests

`$ kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml`

3. Expose load balancer or portforward so we can access it locally
`$ kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'`

Another option is portforward:
`$ kubectl port-forward svc/argocd-server -n argocd 8080:443`

1. Grab default admin password
   
`$ argocd admin initial-password -n argocd`

Since this is stored as a kubernetes secret (and should be deleted on prod), so it can also be fetched by

`$ kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d`

***

## 2. Vault

### Vault installation
<br>

1. Add the vault helm repo

`$ helm repo add hashicorp https://helm.releases.hashicorp.com`

2. Update helm repos

`$ helm repo update`

3. ONLY LOCALLY - create a raft config so we can use regular storage with minikube

```sh
$ cat > helm-vault-raft-values.yml <<EOF
server:
  affinity: ""
  #disable high availability
  ha:
    enabled: false
    raft: 
      enabled: true
EOF
```

4. Install vault

`$ helm install vault hashicorp/vault --values helm-vault-raft-values.yml`

5. Initialize vault-0 with one key share and one key threshold
Note: Production should not run with 1 key/key share

`$ kubectl exec vault-0 -- vault operator init \
    -key-shares=1 \
    -key-threshold=1 \
    -format=json > cluster-keys.json`

6. Optionally display unseal key (requires `jq` to be installed):

`jq -r ".unseal_keys_b64[]" cluster-keys.json`

7. Create an env variable called `VAULT_UNSEAL_KEY`

`$ VAULT_UNSEAL_KEY=$(jq -r ".unseal_keys_b64[]" cluster-keys.json)`

8. Unseal the vault pod

`$ kubectl exec vault-0 -- vault operator unseal $VAULT_UNSEAL_KEY`

### Setting secrets in Vault:

1. Get root token for initial access (should be deleted in production after creating actual roles)

```sh
jq -r ".root_token" cluster-keys.json

hvs.aoRaTtifpewhVh6BcaYlmWgA
```

1. Exec into pod and login

```sh
$ kubectl exec --stdin=true --tty=true vault-0 -- /bin/sh
$ vault login

Token (will be hidden): 
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                hvs.aoRaTtifpewhVh6BcaYlmWgA
token_accessor       QsPsY3vwVFV6xuRnODsBmoGv
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```

2. Enable the kv-v2 engine

```sh
$ vault secrets enable -path=secret kv-v2
Success! Enabled the kv-v2 secrets engine at: secret/
```

3. Create your secret

```sh
$ vault kv put secret/webapp/config username="static-user" password="static-password"

====== Secret Path ======
secret/data/webapp/config

======= Metadata =======
Key                Value
---                -----
created_time       2022-06-07T05:15:19.402740412Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1
```

4. Configure kubernetes auth

```sh
$ vault auth enable kubernetes

Success! Enabled kubernetes auth method at: kubernetes/
```

5. Configure the Kubernetes authentication method to use the location of the Kubernetes API

```sh
$ vault write auth/kubernetes/config \
    kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443"

Success! Data written to: auth/kubernetes/config
```

6. Create a policy that allows reading our new secret

```sh
$ vault policy write webapp - <<EOF
path "secret/data/webapp/config" {
  capabilities = ["read"]
}
EOF

Success! Uploaded policy: webapp
```

"*" policy for argo (should not be done in prod obviously)

```sh
$ vault policy write argocd - <<EOF
path "secret/*" {
  capabilities = ["create", "read", "update", "patch", "delete", "list"]
}
EOF

Success! Uploaded policy: argocd
```

7. Create a Kubernetes authentication role, named webapp, that connects the Kubernetes service account name and webapp policy.

```sh
$ vault write auth/kubernetes/role/webapp \
        bound_service_account_names=vault \
        bound_service_account_namespaces=default \
        policies=webapp \
        ttl=24h

Success! Data written to: auth/kubernetes/role/webapp
```

Argocd authentication role:
```sh
$ vault write auth/kubernetes/role/argocd \
    bound_service_account_names=argocd-repo-server \
    bound_service_account_namespaces=argocd \
    policies=argocd \
    ttl=24h
```
8. Run a deployment that tests our Vault auth is setup correctly (the method used in this deployment is not the one that will be used with the ArgoCD vault plugin*)

```yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp
  labels:
    app: webapp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webapp
  template:
    metadata:
      labels:
        app: webapp
    spec:
      serviceAccountName: vault
      containers:
        - name: app
          image: hashieducation/simple-vault-client:latest
          imagePullPolicy: Always
          env:
            - name: VAULT_ADDR
              value: 'http://vault:8200'
            - name: JWT_PATH
              value: '/var/run/secrets/kubernetes.io/serviceaccount/token'
            - name: SERVICE_PORT
              value: '8080'
```

`JWT_PATH` sets the path of the JSON web token (JWT) issued by Kubernetes. This token is used by the web application to authenticate with Vault.

`VAULT_ADDR` sets the address of the Vault service. The Helm chart defined a Kubernetes service named vault that forwards requests to its endpoints (i.e. The pods named vault-0, vault-1, and vault-2).

`SERVICE_PORT` sets the port that the service listens for incoming HTTP requests.

9. Portforward to the deployment and open it in the web-browser:
`kubectl port-forward deployment/webapp 8081:8080`

Opening the webpage should display all the secrets we put in vault - if it doesn't work then it is not configured correctly

***

## 3. Argocd Vault Plugin

### AVP Installation (Sidecar)
<br>

1. Create the cmp-configmap:
`$ kubectl apply -f argocd-vault-plugin/configmap/cmp-plugin-cm.yaml`

2. Patch the argocd-repo-server deployment
`$ kubectl patch -n argocd deployment argocd-repo-server --patch-file argocd-vault-plugin/patch/argocd-repo-server-patch.yaml`

3. Verify that the patch was applied correctly (argocd-repo-server pod should now have 2 containers)
`$ kubectl get pod -n argocd`

4. Setup the connection config between AVP and Vault

The minimum connection variables that need to be set are:

```
VAULT_ADDR
AVP_TYPE
AVP_AUTH_TYPE
AVP_K8S_ROLE
```

`VAULT_ADDR` would be the address of the vault service

In our case this would be `http://vault.default:8200`
`.default` because vault is running in the `default` namespace while argocd's containers are running in the `argocd` namespace

`AVP_TYPE` - the type of secret manager we are using - in this case it is `vault`

`AVP_AUTH_TYPE` - we're using kubernetes auth, so the value would be `k8s`

`AVP_K8S_ROLE` - the name of the role we created in vault - `argocd` (Vault section, step 7)

Now we can create a secret holding these values:

```yaml
apiVersion: v1
data:
  VAULT_ADDR: aHR0cDovL3ZhdWx0LmRlZmF1bHQ6ODIwMA==
  AVP_TYPE: dmF1bHQ=
  AVP_AUTH_TYPE: azhz
  AVP_K8S_ROLE: YXJnb2Nk
kind: Secret
metadata:
  name: vault-configuration
  namespace: argocd
type: Opaque
```

Then, in the `argocd-repo-server` deployment, we need to load these values in the `avp` container

* Optionally use the provided patch `argocd-repo-server-avp-config-patch.yaml`

`kubectl patch -n argocd deployment argocd-repo-server --patch-file argocd-vault-plugin/patch/argocd-repo-server-avp-config-patch.yaml`

in the `env` field:

```yaml
env:
  - name: VAULT_ADDR
    valueFrom:
      secretKeyRef:
        name: vault-configuration
        key: VAULT_ADDR
  - name: AVP_TYPE
    valueFrom:
      secretKeyRef:
        name: vault-configuration
        key: AVP_TYPE
  - name: AVP_AUTH_TYPE
    valueFrom:
      secretKeyRef:
        name: vault-configuration
        key: AVP_AUTH_TYPE
  - name: AVP_K8S_ROLE
    valueFrom:
      secretKeyRef:
        name: vault-configuration
        key: AVP_K8S_ROLE
```

## 4. Create a demo application to verify that substitution works!

1. Create a demo app

`$ argocd app create secret-demo --repo https://gitlab.com/saifxhatem/argocd-vault-demo-secret.git --path k8s --dest-server https://kubernetes.default.svc --dest-namespace default --sync-policy auto`

2. Check that the application state is `healthy` and `synced`

3. Check the value of the secrets using this handy command:

```sh
$ kubectl get secret example-secret -o go-template='
{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}'

sample-secret: static-user
sample-secret-two: static-password

```

